# Generated by Django 3.2.14 on 2022-08-07 21:18

import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("program", "0035_auto_20220807_2312"),
    ]

    operations = [
        migrations.AddField(
            model_name="show",
            name="created_at",
            field=models.DateTimeField(
                auto_now_add=True, default=django.utils.timezone.now
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="show",
            name="created_by",
            field=models.CharField(default="root", max_length=150),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="show",
            name="updated_at",
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name="show",
            name="updated_by",
            field=models.CharField(default="root", max_length=150),
            preserve_default=False,
        ),
    ]
