from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "deletes an user"

    def add_arguments(self, parser):
        parser.add_argument(
            "--username",
            action="store",
            help="Specifies the username.",
            required=True,
            type=str,
        ),

    def handle(self, *args, **options):
        username = options.get("username", None)

        if not username:
            raise CommandError("You must use --username.")
        try:
            User.objects.get(username=username).delete()
        except User.DoesNotExist:
            raise "user does not exist."
        else:
            self.stdout.write(self.style.SUCCESS("user deleted successfully."))
