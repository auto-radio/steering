import pytest

from conftest import assert_data
from program.tests.factories import ShowFactory

pytestmark = pytest.mark.django_db


def url(show=None) -> str:
    return f"/api/v1/shows/{show.id}/" if show else "/api/v1/shows/"


def show_data(funding_category, type_) -> dict[str, str | int]:
    return {
        "funding_category_id": funding_category.id,
        "name": "NAME",
        "short_description": "SHORT DESCRIPTION",
        "slug": "SLUG",
        "type_id": type_.id,
    }


def test_create_show(admin_api_client, funding_category, type_):
    data = show_data(funding_category, type_)

    response = admin_api_client.post(url(), data=data)

    assert response.status_code == 201

    assert_data(response, data)


def test_create_show_forbidden_for_common_user(common_api_client1, funding_category, type_):
    data = show_data(funding_category, type_)

    response = common_api_client1.post(url(), data=data)

    assert response.status_code == 403


def test_delete_show(admin_api_client, show):
    response = admin_api_client.delete(url(show))

    assert response.status_code == 204


def test_delete_show_forbidden_for_common_user(common_api_client1, show):
    response = common_api_client1.delete(url(show))

    assert response.status_code == 403


def test_list_shows(api_client):
    SHOWS = 3
    ShowFactory.create_batch(size=SHOWS)

    response = api_client.get(url())

    assert response.status_code == 200
    assert len(response.data) == SHOWS


def test_retrieve_show_as_admin_user(admin_api_client, show):
    response = admin_api_client.get(url(show))

    assert response.status_code == 200
    assert response.data["id"] == show.id
    assert "internal_note" in response.data


def test_update_show(admin_api_client, funding_category, type_, show):
    update = show_data(funding_category, type_)

    response = admin_api_client.put(url(show), data=update)

    assert response.status_code == 200

    assert_data(response, update)


def test_update_show_forbidden_for_common_user(common_api_client1, funding_category, type_, show):
    update = show_data(funding_category, type_)

    response = common_api_client1.put(url(show), data=update)

    assert response.status_code == 403
