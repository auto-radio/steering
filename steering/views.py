from rest_framework.exceptions import APIException
from rest_framework.views import exception_handler


def full_details_exception_handler(exc, context):
    if isinstance(exc, APIException):
        exc.detail = exc.get_full_details()
    return exception_handler(exc, context)
