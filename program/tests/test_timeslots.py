import pytest

pytestmark = pytest.mark.django_db


def url(show=None, schedule=None, timeslot=None) -> str:
    if show and schedule and timeslot:
        return f"/api/v1/shows/{show.id}/schedules/{schedule.id}/timeslots/{timeslot.id}/"
    elif show and schedule:
        return f"/api/v1/shows/{show.id}/schedules/{schedule.id}/timeslots/"
    elif show:
        return f"/api/v1/shows/{show.id}/schedules/"
    elif timeslot:
        return f"/api/v1/timeslots/{timeslot.id}/"
    else:
        return "/api/v1/timeslots/"


def timeslot_data() -> dict[str, str | int]:
    return {
        "memo": "MEMO",
        "playlist_id": 1,
        "repetition_of": 1,
    }


def test_delete_timeslot_as_admin(admin_api_client, once_timeslot):
    response = admin_api_client.delete(url(timeslot=once_timeslot))

    assert response.status_code == 204


def test_delete_forbidden_as_common_user(common_api_client1, once_timeslot):
    response = common_api_client1.delete(url(timeslot=once_timeslot))

    assert response.status_code == 403


def test_delete_forbidden_as_unauthenticated_user(api_client, once_timeslot):
    response = api_client.delete(url(timeslot=once_timeslot))

    assert response.status_code == 403


def test_retrieve_timeslot(api_client, once_timeslot):
    response = api_client.get(url(timeslot=once_timeslot))

    assert response.status_code == 200


def test_update_memo_as_admin(admin_api_client, once_timeslot):
    update = {"memo": "MEMO"}

    response = admin_api_client.patch(url(timeslot=once_timeslot), data=update)

    assert response.status_code == 200


def test_update_playlist_id_as_admin(admin_api_client, once_timeslot):
    update = {"playlist_id": 1}

    response = admin_api_client.patch(url(timeslot=once_timeslot), data=update)

    assert response.status_code == 200


def test_update_repetition_of_as_admin(admin_api_client, once_timeslot):
    update = {"repetition_of": 1}
    response = admin_api_client.patch(url(timeslot=once_timeslot), data=update)

    assert response.status_code == 200


def test_update_as_admin(admin_api_client, once_timeslot):
    update = timeslot_data()

    response = admin_api_client.put(url(timeslot=once_timeslot), data=update)

    assert response.status_code == 200


def test_update_forbidden_as_not_owner(common_api_client2, owned_show_once_timeslot):
    update = timeslot_data()

    response = common_api_client2.put(url(timeslot=owned_show_once_timeslot), data=update)

    assert response.status_code == 403


def test_update_timeslot_forbidden_as_unauthenticated(api_client, once_timeslot):
    update = timeslot_data()

    response = api_client.put(url(timeslot=once_timeslot), data=update)

    assert response.status_code == 403
