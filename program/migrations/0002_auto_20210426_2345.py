# Generated by Django 2.2.20 on 2021-04-26 21:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("program", "0001_squashed"),
    ]

    operations = [
        migrations.RenameField(
            model_name="schedule", old_name="fallback_id", new_name="default_id"
        ),
        migrations.RenameField(
            model_name="show", old_name="fallback_id", new_name="default_id"
        ),
    ]
