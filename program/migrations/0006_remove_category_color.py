# Generated by Django 2.2.25 on 2022-01-11 23:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("program", "0005_auto_20220111_2251"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="category",
            name="color",
        ),
    ]
