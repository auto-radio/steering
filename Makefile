POETRY = poetry
POETRY_RUN = $(POETRY) run
POETRY_RUN_MANAGE = $(POETRY_RUN) ./manage.py

.DEFAULT_GOAL := run.prod

ifndef TANK_CALLBACK_BASE_URL
override TANK_CALLBACK_BASE_URL = "${AURA_PROTO}://${AURA_HOST}/tank"
endif

ifndef DASHBOARD_CALLBACK_BASE_URL
override DASHBOARD_CALLBACK_BASE_URL = "${AURA_PROTO}://${AURA_HOST}"
endif

create_oidc_client.dashboard:
	$(POETRY_RUN_MANAGE) create_oidc_client dashboard public --client-id ${DASHBOARD_OIDC_CLIENT_ID} --client-secret ${DASHBOARD_OIDC_CLIENT_SECRET} -r "id_token token" -u ${DASHBOARD_CALLBACK_BASE_URL}/oidc_callback.html -u ${DASHBOARD_CALLBACK_BASE_URL}/oidc_callback_silentRenew.html -p ${DASHBOARD_CALLBACK_BASE_URL} -p ${DASHBOARD_CALLBACK_BASE_URL}/

create_oidc_client.tank:
	$(POETRY_RUN_MANAGE) create_oidc_client tank confidential --client-id ${TANK_OIDC_CLIENT_ID} --client-secret ${TANK_OIDC_CLIENT_SECRET} -r "code" -u ${TANK_CALLBACK_BASE_URL}/tank/auth/oidc/callback

initialize: migrate loaddata.program create_oidc_client.dashboard create_oidc_client.tank
	$(POETRY_RUN_MANAGE) createsuperuser --no-input
	$(POETRY_RUN_MANAGE) creatersakey

showmigrations:
	$(POETRY_RUN_MANAGE) showmigrations

migrate:
	$(POETRY_RUN_MANAGE) migrate --no-input

collectstatic:
	$(POETRY_RUN_MANAGE) collectstatic --no-input

loaddata.program:
	$(POETRY_RUN_MANAGE) loaddata fixtures/program/*.json

loaddata.sample:
	$(POETRY_RUN_MANAGE) loaddata fixtures/sample/*.json

loaddata.custom:
	$(POETRY_RUN_MANAGE) loaddata fixtures/custom/*.json

loaddata.test:
	$(POETRY_RUN_MANAGE) loaddata fixtures/test/*.json

removestaleimages:
	$(POETRY_RUN_MANAGE) removestaleimages

run.dev: dev.install migrate collectstatic
	$(POETRY_RUN_MANAGE) runserver 0.0.0.0:8000

dev.install:
	$(POETRY) install --no-root

run.prod: migrate collectstatic
	$(POETRY_RUN) gunicorn --bind 0.0.0.0:8000 --workers `nproc` steering.wsgi

run.debug: migrate
	DEBUG=1 $(POETRY_RUN_MANAGE) runserver_plus 0.0.0.0:8000
