from typing import Iterable, Tuple


def _generate_choices_description(choices: Iterable[Tuple[str, str]]):
    def _gen():
        for key, value in choices:
            yield f"**{key}**: {value}\n\n"

    return "\n".join(_gen()).strip()


def add_enum_documentation(result, generator, request, public):
    """
    Choice descriptions are available through the assigned choices values
    but are not incorporated into the schema by default.

    This post-processing hook adds them to the appropriate objects.
    """
    # TODO: The logic behind this might be a worthwhile addition to drf-spectacular.
    from program.models import Schedule
    from program.serializers import SOLUTION_CHOICES

    weekday_choices_desc = _generate_choices_description(
        Schedule.by_weekday.field.choices
    )
    solutions_choices_desc = _generate_choices_description(SOLUTION_CHOICES.items())
    schema = result["components"]["schemas"]
    schema["ByWeekdayEnum"]["description"] = weekday_choices_desc
    schema["SolutionChoicesEnum"]["description"] = solutions_choices_desc
    for item in ["ScheduleCreateUpdateRequest", "PatchedScheduleCreateUpdateRequest"]:
        solutions_props = schema[item]["properties"]["solutions"][
            "additionalProperties"
        ]
        solutions_props["description"] = solutions_choices_desc
    return result


def fix_schedule_pk_type(result, generator, request, public):
    """
    schedule_pk’s type cannot be deduced in note routes because the Note class
    has no schedule field drf-spectacular can map it to.

    Normally we would define this by using @extend_schema on the note viewset, but
    as the schedule_pk field does not exist for __all__ note routes this would
    inadvertently add the field to routes that don’t even have the parameter like
    /api/v1/notes/.

    So we patch the type of schedule_pk fields in post-processing and ignore
    the warning until we find a better solution.
    """
    for path, methods in result["paths"].items():
        if not ("{schedule_pk}" in path and "/note" in path):
            continue
        for method_name, method_def in methods.items():
            for parameter in method_def["parameters"]:
                if parameter["in"] == "path" and parameter["name"] == "schedule_pk":
                    parameter["schema"]["type"] = "integer"
                    break
    return result
