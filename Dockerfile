FROM python:3.11-slim-bullseye AS base

ENV AURA_UID=2872
ENV POETRY_CACHE_DIR=/app/.cache
ENV POETRY_HOME=/opt/poetry
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PATH="${POETRY_HOME}/bin:${PATH}"

WORKDIR /app

COPY poetry.lock pyproject.toml /app/

RUN apt-get update && apt-get install -y curl gcc graphviz ldap-utils libldap2-dev libmagic1 libsasl2-dev make
RUN python -m venv ${POETRY_HOME}
RUN pip install poetry==1.7.0
RUN poetry install

EXPOSE 8000

FROM base AS dev

COPY . .

VOLUME ["/app"]

RUN adduser --home /app --no-create-home --system --uid ${AURA_UID} --group app
RUN mkdir -p /app/logs
RUN chown -R app:app /app

USER app

# run with Django's development server
CMD ["run.dev"]

FROM base AS prod

COPY . .

# run with gunicorn
CMD ["run.prod"]

ENTRYPOINT ["make"]
