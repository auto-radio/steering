import os
from pathlib import Path

import ldap
from corsheaders.defaults import default_headers
from django_auth_ldap.config import LDAPSearch, PosixGroupType

BASE_DIR = Path(__file__).resolve(strict=True).parent.parent

LOCALE_PATHS = (BASE_DIR / "locale",)

MEDIA_ROOT = BASE_DIR / "site_media"
MEDIA_URL = "/site_media/"

STATIC_ROOT = BASE_DIR / "static"
STATIC_URL = "/static/"

ROOT_URLCONF = "steering.urls"

DEBUG = True if os.getenv("DEBUG") else False
SITE_ID = 1

# Must be set if DEBUG is False
ALLOWED_HOSTS = os.getenv("ALLOWED_HOSTS", default="127.0.0.1,localhost").split(",")

CORS_ALLOWED_ORIGINS = ["http://localhost:8080", "http://localhost:8040"]
CORS_ALLOW_CREDENTIALS = True
CORS_ALLOW_HEADERS = list(default_headers) + [
    "content-disposition",
]

DATABASES = {
    "default": {
        "ENGINE": f"django.db.backends.{os.getenv('DATABASE_ENGINE', default='sqlite3')}",
        "NAME": os.getenv("POSTGRES_DB", default=BASE_DIR / "db.sqlite3"),
        "USER": os.getenv("POSTGRES_USER", default="steering"),
        "PASSWORD": os.getenv("POSTGRES_PASSWORD", default="aura"),
        "HOST": os.getenv("POSTGRES_HOST", default="steering-postgres"),
        "PORT": os.getenv("POSTGRES_PORT", default="5432"),
    },
}

TIME_ZONE = os.getenv("TZ", default="Europe/Vienna")
LANGUAGE_CODE = "de"
USE_TZ = True  # django-oidc-provider needs timezones in database

SECRET_KEY = os.getenv("SECRET_KEY", default="secret-key")

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.request",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

MIDDLEWARE = (
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "djangorestframework_camel_case.middleware.CamelCaseMiddleWare",
)

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly"
    ],
    "DEFAULT_AUTHENTICATION_CLASSES": [
        "program.auth.OidcOauth2Auth",
    ],
    "DEFAULT_FILTER_BACKENDS": ["django_filters.rest_framework.DjangoFilterBackend"],
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    "DEFAULT_PARSER_CLASSES": (
        "djangorestframework_camel_case.parser.CamelCaseFormParser",
        "djangorestframework_camel_case.parser.CamelCaseMultiPartParser",
        "djangorestframework_camel_case.parser.CamelCaseJSONParser",
    ),
    "DEFAULT_RENDERER_CLASSES": (
        "djangorestframework_camel_case.render.CamelCaseJSONRenderer",
        "djangorestframework_camel_case.render.CamelCaseBrowsableAPIRenderer",
    ),
    "EXCEPTION_HANDLER": "steering.views.full_details_exception_handler",
}

SPECTACULAR_SETTINGS = {
    "TITLE": "AURA Steering API",
    "DESCRIPTION": "Programme/schedule management for Aura",
    "POSTPROCESSING_HOOKS": [
        "drf_spectacular.hooks.postprocess_schema_enums",
        "steering.schema.add_enum_documentation",
        "steering.schema.fix_schedule_pk_type",
        "drf_spectacular.contrib.djangorestframework_camel_case.camelize_serializer_fields",
    ],
    "VERSION": "1.0.0",
}

INSTALLED_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.admin",
    "django.contrib.staticfiles",
    "program",
    "versatileimagefield",
    "rest_framework",
    "rest_framework_nested",
    "django_filters",
    "django_extensions",
    "drf_spectacular",
    "oidc_provider",
    "corsheaders",
)

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

# Set the desired sizes for your thumbnails (px)
# Will apply to all uploaded images
THUMBNAIL_SIZES = ["640x480", "200x200", "150x150"]

# When generating schedules/timeslots:
# If last_date wasn't set manually, add timeslots until these number of days after the start_date
AUTO_SET_LAST_DATE_TO_DAYS_IN_FUTURE = 365

# If last_date wasn't set manually, add timeslots until the end of the current year
# Overrides the above setting if True
AUTO_SET_LAST_DATE_TO_END_OF_YEAR = True

# URL to CBA - Cultural Broadcasting Archive
CBA_URL = "https://cba.fro.at"

# Contact cba@fro.at to get whitelisted and get an API KEY
# Leave empty to disable requests to CBA
CBA_API_KEY = os.getenv("CBA_API_KEY", default="")

# URL to CBA ajax handler (used for retrieving additional data or increasing stream counter)
CBA_AJAX_URL = CBA_URL + "/wp-admin/admin-ajax.php"

# URL to CBA's REST API with trailing slash
CBA_REST_API_URL = CBA_URL + "/wp-json/wp/v2/"

# OIDC Provider Settings
LOGIN_URL = "/admin/login/"  # Login page OIDC redirects to
OIDC_EXTRA_SCOPE_CLAIMS = "steering.oidc_provider_settings.AuraScopeClaims"

# WSGI_APPLICATION = 'steering.wsgi.application';

if os.getenv("USE_LDAP_AUTH"):
    AUTHENTICATION_BACKENDS = (
        "django_auth_ldap.backend.LDAPBackend",
        "django.contrib.auth.backends.ModelBackend",
    )

    AUTH_LDAP_SERVER_URI = "ldap://ldap.local"
    AUTH_LDAP_BIND_DN = "cn=reader,dc=local"
    AUTH_LDAP_BIND_PASSWORD = os.getenv("AUTH_LDAP_BIND_PASSWORD")
    AUTH_LDAP_USER_DN_TEMPLATE = "uid=%(user)s,ou=users,dc=local"
    AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
        "ou=groups,dc=local",
        ldap.SCOPE_SUBTREE,
        "(objectClass=posixGroup)",
    )
    AUTH_LDAP_GROUP_TYPE = PosixGroupType()
    AUTH_LDAP_USER_ATTR_MAP = {
        "first_name": "givenName",
        "last_name": "sn",
        "email": "mail",
    }
    AUTH_LDAP_USER_FLAGS_BY_GROUP = {
        "is_active": "cn=active,ou=django,ou=groups,dc=local",
        "is_staff": "cn=staff,ou=django,ou=groups,dc=local",
        "is_superuser": "cn=superuser,ou=django,ou=groups,dc=local",
    }

    AUTH_LDAP_ALWAYS_UPDATE_USER = True
    AUTH_LDAP_FIND_GROUP_PERMS = True
    AUTH_LDAP_MIRROR_GROUPS = True

# SITE_URL is used by django-oidc-provider and openid-configuration will break not set correctly
PORT = os.getenv("STEERING_PORT")
AURA_PROTO = os.getenv("AURA_PROTO", default="http")
AURA_HOST = os.getenv("AURA_HOST", default="localhost")

SITE_URL = f"{AURA_PROTO}://{AURA_HOST}:{PORT}" if PORT else f"{AURA_PROTO}://{AURA_HOST}"

if AURA_PROTO == "https":
    CSRF_TRUSTED_ORIGINS = [f"{AURA_PROTO}://{AURA_HOST}"]

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "django.server": {
            "()": "django.utils.log.ServerFormatter",
            "format": "[{server_time}] {message}",
            "style": "{",
        }
    },
    "handlers": {
        "file": {
            "class": "logging.FileHandler",
            "filename": os.path.abspath(os.path.join(BASE_DIR, "logs", "steering.log")),
            "formatter": "django.server",
        },
    },
    "loggers": {
        "django": {
            "handlers": ["file"],
            "level": os.getenv("STEERING_LOG_LEVEL", "INFO"),
            "propagate": True,
        },
    },
}
