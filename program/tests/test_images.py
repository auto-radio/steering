import pytest

from conftest import assert_data
from program.tests.factories import ImageFactory

pytestmark = pytest.mark.django_db


def url(image=None) -> str:
    return f"/api/v1/images/{image.id}/" if image else "/api/v1/images/"


def image_data() -> dict[str, str]:
    return {
        "alt_text": "ALT TEXT",
        "credits": "CREDITS",
        "ppoi": "0.5x0.5",
    }


def test_create_image(image_file, common_api_client1):
    data = {"image": image_file}

    response = common_api_client1.post(url(), data=data)

    assert response.status_code == 201


def test_delete_image(owned_image, common_api_client1):
    response = common_api_client1.delete(url(owned_image))

    assert response.status_code == 204


def test_delete_image_not_found_for_different_user(owned_image, common_api_client2):
    response = common_api_client2.delete(url(owned_image))

    assert response.status_code == 404


def test_list_images(image_file, common_user1, common_api_client1):
    IMAGES = 3
    ImageFactory.create_batch(size=IMAGES, image=image_file, owner=common_user1)

    response = common_api_client1.get(url())

    assert response.status_code == 200
    assert len(response.data) == IMAGES


def test_list_images_for_different_user(image_file, common_user1, common_api_client2):
    IMAGES = 3
    ImageFactory.create_batch(size=IMAGES, image=image_file, owner=common_user1)

    response = common_api_client2.get(url())

    assert response.status_code == 200
    assert len(response.data) == 0


def test_retrieve_image(owned_image, common_api_client1):
    response = common_api_client1.get(url(owned_image))

    assert response.status_code == 200


def test_retrieve_image_not_found_for_different_user(owned_image, common_api_client2):
    response = common_api_client2.get(url(owned_image))

    assert response.status_code == 404


def test_update_alt_text(owned_image, common_api_client1):
    update = {"alt_text": "ALT_TEXT"}

    response = common_api_client1.patch(url(owned_image), data=update)

    assert response.status_code == 200

    assert_data(response, update)


def test_update_alt_text_not_found_for_different_user(owned_image, common_api_client2):
    update = {"alt_text": "ALT_TEXT", "credits": "CREDITS"}

    response = common_api_client2.patch(url(owned_image), data=update)

    assert response.status_code == 404


def test_update_credits(owned_image, common_api_client1):
    update = {"credits": "CREDITS"}

    response = common_api_client1.patch(url(owned_image), data=update)

    assert response.status_code == 200

    assert_data(response, update)


def test_update_credits_not_found_for_different_user(owned_image, common_api_client2):
    update = {"credits": "CREDITS"}

    response = common_api_client2.patch(url(owned_image), data=update)

    assert response.status_code == 404


def test_update_ppoi(owned_image, common_api_client1):
    update = {"ppoi": "0.7x0.3"}

    response = common_api_client1.patch(url(owned_image), data=update)

    assert response.status_code == 200

    assert_data(response, update)


def test_update_ppoi_not_found_for_different_user(owned_image, common_api_client2):
    update = {"ppoi": "0.7x0.3"}

    response = common_api_client2.patch(url(owned_image), data=update)

    assert response.status_code == 404


def test_set_image_license(owned_image, common_api_client1, public_domain_license):
    update = {"license_id": public_domain_license.id}

    response = common_api_client1.patch(url(owned_image), data=update)

    assert response.status_code == 200

    assert_data(response, update)


def test_unset_image_license(owned_licensed_image, common_api_client1):
    update = {"license_id": None}

    response = common_api_client1.patch(url(owned_licensed_image), data=update, format="json")

    assert response.status_code == 200

    assert_data(response, update)
