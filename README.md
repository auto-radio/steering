# AURA Steering: Program Scheduler

AURA Steering is the scheduling module, where the actual program schedule of
the whole station is stored as well as all infos regarding single shows and
emissions.

It also acts as an OpenID Connect provider for `dashboard` and `tank`.

## Installation

Follow the instructions to deploy AURA Web from the [Docker Compose
Installation](https://docs.aura.radio/en/latest/administration/install-docker-compose.html)

If you just want to try `steering` locally, use the provided compose setup and
be sure to add:

```dotenv
AURA_PROTO=http
```

to the `.env` file.

## Configuration for development

A better development setup for `steering`, that pulls the docker images for
`dashboard`, `dashboard-clock` and `tank` from Docker Hub, builds the image for
`steering` locally and exposes the ports can be achieved with
`docker-compose.steering.yml` by adding this lines to the end of the `.env`
file:

```dotenv
STEERING_VERSION=local
TANK_VERSION=unstable
DASHBOARD_VERSION=unstable
DASHBOARD_CLOCK_VERSION=unstable
COMPOSE_FILE=docker-compose.yml:docker-compose.steering.yml
```

## LDAP authentication backend

If you would like to add LDAP authentication backend, you need to enable it by
setting the `STEERING_USE_LDAP_AUTH` and the `AUTH_LDAP_BIND_PASSWORD`
environment variables in the `.env` file and configure the values of the
`AUTH_LDAP_*` settings in
https://gitlab.servus.at/aura/steering/-/blob/master/steering/settings.py#L198

```dotenv
STEERING_USE_LDAP_AUTH=1
AUTH_LDAP_BIND_PASSWORD=
```

## Testing

If you want to run the test suite, you can run `pytest` in the container:

```shell
$ docker compose run steering poetry run pytest
```

## Installation and execution with Poetry

If you really don't want to use Docker, you can install and run `steering`
using [Poetry](https://python-poetry.org/) using SQLite.

Refer to the [documentation](https://python-poetry.org/docs/#installation) on
how to install Poetry.

First, install all the dependencies for the project:

```shell
$ poetry install --no-root
```

Then, you can set up the database:

```shell
$ poetry run ./manage.py migrate
```

And, load the fixtures:

```shell
$ poetry run ./manage.py loaddata fixtures/*/*.json
```

Then, you can create an RSA Key and the clients for `dashboard` and `tank`:

```shell
$ poetry run ./manage.py creatersakey
$ poetry run ./manage.py create_oidc_client dashboard public -r "id_token token" -u http://localhost:8080/oidc_callback.html -u http://localhost:8080/oidc_callback_silentRenew.html -p http://localhost:8080/
$ poetry run ./manage.py create_oidc_client tank confidential -r "code" -u http://localhost:8040/auth/oidc/callback
```

Finally, you can start the development server:

```shell
$ poetry run ./manage.py runserver
```

### Extending the settings

If you want to extend the settings while doing local development with poetry,
you just need to set the `DJANGO_SETTINGS_MODULE` environment variable to point
to the new settings file, e.g. to run the server using the `sample_settings.py`
file:

```shell
$ DJANGO_SETTINGS_MODULE=steering.sample_settings poetry run ./manage.py runserver
```

## Data Model

A visualization of the data model can be generated using the [Graph Models
Django
Extensions](https://django-extensions.readthedocs.io/en/latest/graph_models.html)

The following command will generate an image out of the models:

```shell
$ poetry run ./manage.py graph_models --pydot -g -o steering_data_model.png program profile
```
