# Generated by Django 4.2.2 on 2023-07-12 22:56

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("program", "0062_alter_image_height_alter_image_width_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="note",
            name="timeslot",
            field=models.OneToOneField(
                null=True, on_delete=django.db.models.deletion.SET_NULL, to="program.timeslot"
            ),
        ),
        migrations.AlterField(
            model_name="userprofile",
            name="user",
            field=models.OneToOneField(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="profile",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
