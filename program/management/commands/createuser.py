from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "creates an user"

    def add_arguments(self, parser):
        parser.add_argument(
            "--username",
            action="store",
            help="Specifies the username.",
            required=True,
            type=str,
        )
        parser.add_argument(
            "--email",
            action="store",
            help="Specifies the email address.",
            required=True,
            type=str,
        )

    def handle(self, *args, **options):
        username = options.get("username", None)
        email = options.get("email", None)

        if not username or not email:
            raise CommandError("You must use --username and --email.")
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            User.objects.create_user(username=username, email=email)
            self.stdout.write(self.style.SUCCESS("user created successfully."))
        else:
            self.stdout.write(
                self.style.NOTICE("User already exists, no need to create.")
            )
