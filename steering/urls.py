#
# steering, Programme/schedule management for AURA
#
# Copyright (C) 2011-2017, 2020, Ernesto Rico Schmidt
# Copyright (C) 2017-2019, Ingo Leindecker
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework_nested import routers

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from program.views import (
    APICategoryViewSet,
    APIFundingCategoryViewSet,
    APIHostViewSet,
    APIImageViewSet,
    APILanguageViewSet,
    APILicenseViewSet,
    APILinkTypeViewSet,
    APIMusicFocusViewSet,
    APINoteViewSet,
    APIRRuleViewSet,
    APIScheduleViewSet,
    APIShowViewSet,
    APITimeSlotViewSet,
    APITopicViewSet,
    APITypeViewSet,
    APIUserViewSet,
    json_day_schedule,
    json_playout,
)

admin.autodiscover()

router = routers.DefaultRouter()
router.register(r"users", APIUserViewSet)
router.register(r"hosts", APIHostViewSet)
router.register(r"shows", APIShowViewSet)
router.register(r"schedules", APIScheduleViewSet)
router.register(r"timeslots", APITimeSlotViewSet)
router.register(r"notes", APINoteViewSet)
router.register(r"categories", APICategoryViewSet)
router.register(r"topics", APITopicViewSet)
router.register(r"types", APITypeViewSet)
router.register(r"music-focus", APIMusicFocusViewSet)
router.register(r"funding-categories", APIFundingCategoryViewSet)
router.register(r"languages", APILanguageViewSet)
router.register(r"licenses", APILicenseViewSet)
router.register(r"link-types", APILinkTypeViewSet)
router.register(r"rrules", APIRRuleViewSet)
router.register(r"images", APIImageViewSet)

# Nested Routers

show_router = routers.NestedSimpleRouter(router, r"shows", lookup="show")

# /shows/1/schedules
show_router.register(r"schedules", APIScheduleViewSet, basename="show-schedules")

# /shows/1/notes
show_router.register(r"notes", APINoteViewSet, basename="show-notes")

# /shows/1/timeslots
show_router.register(r"timeslots", APITimeSlotViewSet, basename="show-timeslots")
show_timeslot_router = routers.NestedSimpleRouter(show_router, r"timeslots", lookup="timeslot")

# /shows/1/timeslots/1/note/
show_timeslot_router.register(r"note", APINoteViewSet, basename="show-timeslots-note")

# /shows/1/schedules
schedule_router = routers.NestedSimpleRouter(show_router, r"schedules", lookup="schedule")

# /shows/1/schedules/1/timeslots
schedule_router.register(r"timeslots", APITimeSlotViewSet, basename="schedule-timeslots")
timeslot_router = routers.NestedSimpleRouter(schedule_router, r"timeslots", lookup="timeslot")

# /shows/1/schedules/1/timeslots/1/note
timeslot_router.register(r"note", APINoteViewSet, basename="timeslots-note")

urlpatterns = [
    path("openid/", include("oidc_provider.urls", namespace="oidc_provider")),
    path("api/v1/", include(router.urls)),
    path("api/v1/", include(show_router.urls)),
    path("api/v1/", include(show_timeslot_router.urls)),
    path("api/v1/", include(schedule_router.urls)),
    path("api/v1/", include(timeslot_router.urls)),
    path("api/v1/playout", json_playout),
    path("api/v1/program/week", json_playout),
    path("api/v1/program/<int:year>/<int:month>/<int:day>/", json_day_schedule),
    path("api/v1/schema/", SpectacularAPIView.as_view(), name="schema"),
    path(
        "api/v1/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path("admin/", admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
