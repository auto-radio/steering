from datetime import timedelta

import factory

from django.contrib.auth.models import User
from django.utils.timezone import now
from program.models import (
    FundingCategory,
    Host,
    Image,
    License,
    RRule,
    Schedule,
    Show,
    TimeSlot,
    Type,
)


class CommonUserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    password = "password"
    username = factory.Sequence(lambda n: "common_%d" % n)


class HostFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Host

    name = factory.Sequence(lambda n: "host %d" % n)


class ImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Image


class FundingCategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = FundingCategory

    name = factory.Sequence(lambda n: "funding category %d" % n)
    slug = factory.Sequence(lambda n: "fc_%d" % n)


class TypeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Type

    name = factory.Sequence(lambda n: "type %d" % n)
    slug = factory.Sequence(lambda n: "t_%d" % n)


class ShowFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Show

    name = factory.Sequence(lambda n: "show %d" % n)
    slug = factory.Sequence(lambda n: "%s_d" % n)


class RRuleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = RRule


class ScheduleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Schedule

    end_time = "03:00:00"
    first_date = "2023-01-01"
    start_time = "02:00:00"


class TimeslotFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TimeSlot

    end = now() + timedelta(hours=1)
    start = now()


class LicenseFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = License

    identifier = "pd"
    name = "Public Domain"
