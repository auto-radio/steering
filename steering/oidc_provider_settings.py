#
# steering, Programme/schedule management for AURA
#
# Copyright (C) 2011-2017, 2020, Ernesto Rico Schmidt
# Copyright (C) 2017-2019, Ingo Leindecker
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from oidc_provider.lib.claims import ScopeClaims

from django.utils.translation import gettext as _


class AuraScopeClaims(ScopeClaims):
    info_username = (
        _("username"),
        _("Your username."),
    )

    def scope_username(self):
        dic = {
            "username": self.user.username,
            # 'privileged': (self.user.is_staff or self.user.is_superuser)
            "privileged": self.user.is_superuser,
        }

        return dic

    info_aura_shows = (
        _("AURA Shows"),
        _("AURA shows you have access to."),
    )

    def scope_aura_shows(self):
        from program.models import Show

        # TODO: should add filter `is_active=True` ?
        public_show_slugs = list(
            Show.objects.filter(is_public=True).values_list("slug", flat=True)
        )
        show_slugs = list(self.user.shows.all().values_list("slug", flat=True))
        dic = {"shows": show_slugs, "public-shows": public_show_slugs}

        return dic
