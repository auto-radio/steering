import datetime

from django.contrib.auth.models import User
from django.utils.text import slugify
from django.utils.timezone import now
from program.models import Note, RRule, Schedule, Show, TimeSlot


class SteeringTestCaseMixin:
    base_url = "/api/v1"

    def _url(self, *paths, **kwargs):
        url = "/".join(str(p) for p in paths) + "/"
        return f"{self.base_url}/{url.format(**kwargs)}"

    def _get_client(self, user=None):
        client = self.client_class()
        if user:
            client.force_authenticate(user=user)
        return client


class UserMixin:
    user_admin: User
    user_common: User

    def setUp(self):
        self.user_admin = User.objects.create_superuser(
            "admin", "admin@aura.radio", password="admin"
        )
        self.user_common = User.objects.create_user(
            "herbert", "herbert@aura.radio", password="herbert"
        )


class ShowMixin:
    def _create_show(self, name: str, **kwargs):
        kwargs["name"] = name
        kwargs.setdefault("slug", slugify(name))
        kwargs.setdefault("short_description", f"The {name} show")
        owners = kwargs.pop("owners", [])
        show = Show.objects.create(**kwargs)
        if owners:
            show.owners.set(owners)
        return show


class ScheduleMixin:
    def _get_rrule(self):
        rrule = RRule.objects.first()
        if rrule is None:
            rrule = RRule.objects.create(name="once", freq=0)
        return rrule

    def _create_schedule(self, show: Show, **kwargs):
        _first_date = kwargs.get("first_date", now().date())
        kwargs["show"] = show
        kwargs.setdefault("first_date", _first_date)
        kwargs.setdefault("start_time", "08:00")
        kwargs.setdefault("last_date", _first_date + datetime.timedelta(days=365))
        kwargs.setdefault("end_time", "09:00")
        kwargs.setdefault("rrule", self._get_rrule())
        return Schedule.objects.create(**kwargs)


class TimeSlotMixin:
    def _create_timeslot(self, schedule: Schedule, **kwargs):
        _start = kwargs.get("start", now())
        kwargs.setdefault("schedule", schedule)
        kwargs.setdefault("start", _start)
        kwargs.setdefault("end", _start + datetime.timedelta(hours=1))
        return TimeSlot.objects.create(**kwargs)


class NoteMixin:
    def _create_note(self, timeslot: TimeSlot, **kwargs):
        note_count = Note.objects.all().count()
        _title = kwargs.get("title", f"a random note #{note_count}")
        kwargs["timeslot"] = timeslot
        kwargs["title"] = _title
        kwargs.setdefault("slug", slugify(_title))
        return Note.objects.create(**kwargs)

    def _create_random_note_content(self, **kwargs):
        note_count = Note.objects.all().count()
        _title = kwargs.get("title", f"a random note #{note_count}")
        kwargs["title"] = _title
        kwargs.setdefault("slug", slugify(_title))
        kwargs.setdefault("content", "some random content")
        kwargs.setdefault("contributor_ids", [])
        return kwargs


class ProgramModelMixin(ShowMixin, ScheduleMixin, TimeSlotMixin, NoteMixin):
    pass


class BaseMixin(UserMixin, ProgramModelMixin, SteeringTestCaseMixin):
    pass
