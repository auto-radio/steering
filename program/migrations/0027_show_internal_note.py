# Generated by Django 3.2.14 on 2022-08-01 15:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("program", "0026_auto_20220728_2227"),
    ]

    operations = [
        migrations.AddField(
            model_name="show",
            name="internal_note",
            field=models.TextField(blank=True, null=True),
        ),
    ]
