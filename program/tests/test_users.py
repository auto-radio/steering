import pytest

from conftest import assert_data
from program.tests.factories import CommonUserFactory

pytestmark = pytest.mark.django_db


def url(user=None) -> str:
    return f"/api/v1/users/{user.id}/" if user else "/api/v1/users/"


def user_data(is_superuser=False, add_profile=False) -> dict[str, str | dict[str, str]]:
    data = {
        "password": "password",
        "username": "user",
    }

    if is_superuser:
        data["is_superuser"] = is_superuser

    if add_profile:
        data["profile"] = {
            "cba_username": "CBA USERNAME",
            "cba_user_token": "CBA USER TOKEN",
        }

    return data


def test_create_user(admin_api_client):
    data = user_data()

    response = admin_api_client.post(url(), data=data)

    assert response.status_code == 201

    assert_data(response, data)


def test_create_superuser(admin_api_client):
    data = user_data(is_superuser=True)

    response = admin_api_client.post(url(), data=data)

    assert response.status_code == 201

    assert_data(response, data)


def test_create_user_unauthorized(common_api_client1):
    data = user_data()

    response = common_api_client1.post(url(), data=data)

    assert response.status_code == 403


def test_list_users_as_common_user(common_user1, common_api_client1):
    USERS = 3
    CommonUserFactory.create_batch(size=USERS)

    response = common_api_client1.get(url())

    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]["username"] == common_user1.username


def test_list_users_as_other_common_user(common_user1, common_user2, common_api_client2):
    USERS = 3
    CommonUserFactory.create_batch(size=USERS)

    response = common_api_client2.get(url())

    assert response.status_code == 200
    assert len(response.data) == 1
    assert response.data[0]["username"] == common_user2.username


def test_list_users_as_superuser(admin_api_client):
    USERS = 3
    CommonUserFactory.create_batch(size=USERS)

    response = admin_api_client.get(url())

    assert response.status_code == 200
    assert len(response.data) == USERS + 1


def test_list_users_unauthenticated(api_client):
    USERS = 3
    CommonUserFactory.create_batch(size=USERS)

    response = api_client.get(url())

    assert response.status_code == 200
    assert len(response.data) == 0


def test_retrieve_user_as_common_user(common_user1, common_api_client1):
    response = common_api_client1.get(url(common_user1))

    assert response.status_code == 200
    assert response.data["username"] == common_user1.username


def test_retrieve_user_not_found_as_other_common_user(common_user1, common_api_client2):
    response = common_api_client2.get(url(common_user1))

    assert response.status_code == 404


def test_retrieve_user_as_superuser(common_user1, admin_api_client):
    response = admin_api_client.get(url(common_user1))

    assert response.status_code == 200
    assert response.data["username"] == common_user1.username


def test_retrieve_user_not_found_unauthenticated(common_user1, api_client):
    response = api_client.get(url(common_user1))

    assert response.status_code == 404


def test_update_user(common_user1, admin_api_client):
    update = {
        "email": "user@aura.radio",
        "first_name": "FIRST NAME",
        "last_name": "LAST NAME",
    }

    response = admin_api_client.patch(url(common_user1), data=update)

    assert response.status_code == 200

    assert_data(response, update)


def test_update_user_forbidden_as_common_user(common_user1, common_api_client1):
    update = {"username": "USERNAME"}

    response = common_api_client1.patch(url(common_user1), data=update)

    assert response.status_code == 403


def test_update_user_forbidden_as_other_common_user(common_user1, common_api_client2):
    update = {"username": "USERNAME"}

    response = common_api_client2.patch(url(common_user1), data=update)

    assert response.status_code == 403


def test_update_user_forbidden_unauthenticated(common_user1, api_client):
    update = {"username": "USERNAME"}

    response = api_client.patch(url(common_user1), data=update)

    assert response.status_code == 403
