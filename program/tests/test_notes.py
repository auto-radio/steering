from rest_framework.test import APITransactionTestCase
from unittest import skip
from program import tests
from program.models import Schedule, Show


class NoteViewTestCase(tests.BaseMixin, APITransactionTestCase):
    reset_sequences = True

    show_beatbetrieb: Show
    schedule_beatbetrieb: Schedule
    show_musikrotation: Show
    schedule_musikrotation: Schedule

    def setUp(self) -> None:
        super().setUp()
        self.show_beatbetrieb = self._create_show("Beatbetrieb")
        self.schedule_beatbetrieb = self._create_schedule(self.show_beatbetrieb)
        self.show_musikrotation = self._create_show("Musikrotation", owners=[self.user_common])
        self.schedule_musikrotation = self._create_schedule(
            self.show_musikrotation, start_time="10:00", end_time="12:00"
        )

    def test_everyone_can_read_notes(self):
        self._create_note(self._create_timeslot(schedule=self.schedule_beatbetrieb))
        self._create_note(self._create_timeslot(schedule=self.schedule_musikrotation))
        res = self._get_client().get(self._url("notes"))
        self.assertEqual(len(res.data), 2)

    def test_common_users_can_create_notes_for_owned_shows(self):
        ts = self._create_timeslot(schedule=self.schedule_musikrotation)
        client = self._get_client(self.user_common)
        endpoint = self._url("notes")
        res = client.post(
            endpoint, self._create_random_note_content(timeslot_id=ts.id), format="json"
        )
        self.assertEqual(res.status_code, 201)

    def test_common_users_cannot_create_notes_for_foreign_shows(self):
        ts = self._create_timeslot(schedule=self.schedule_beatbetrieb)
        client = self._get_client(self.user_common)
        endpoint = self._url("notes")
        res = client.post(
            endpoint, self._create_random_note_content(timeslot_id=ts.id), format="json"
        )
        self.assertEqual(res.status_code, 404)

    def test_common_user_can_update_owned_shows(self):
        ts = self._create_timeslot(schedule=self.schedule_musikrotation)
        note = self._create_note(ts)
        client = self._get_client(self.user_common)
        new_note_content = self._create_random_note_content(title="meh")
        res = client.put(self._url("notes", note.id), new_note_content, format="json")
        self.assertEqual(res.status_code, 200)

    def test_common_user_cannot_update_notes_of_foreign_shows(self):
        ts = self._create_timeslot(schedule=self.schedule_beatbetrieb)
        note = self._create_note(ts)
        client = self._get_client(self.user_common)
        new_note_content = self._create_random_note_content(title="meh")
        res = client.put(self._url("notes", note.id), new_note_content, format="json")
        self.assertEqual(res.status_code, 404)

    def test_admin_can_create_notes_for_all_timeslots(self):
        timeslot = self._create_timeslot(schedule=self.schedule_musikrotation)
        client = self._get_client(self.user_admin)
        res = client.post(
            self._url("notes"),
            self._create_random_note_content(timeslot_id=timeslot.id),
            format="json",
        )
        self.assertEqual(res.status_code, 201)

    @skip("nested routes are deprecated")
    def test_notes_can_be_created_through_nested_routes(self):
        client = self._get_client(self.user_admin)

        # /shows/{pk}/notes/
        ts1 = self._create_timeslot(schedule=self.schedule_musikrotation)
        url = self._url("shows", self.show_musikrotation.id, "notes")
        note = self._create_random_note_content(title="meh", timeslot_id=ts1.id)
        res = client.post(url, note, format="json")
        self.assertEqual(res.status_code, 201)

        # /shows/{pk}/timeslots/{pk}/note/
        ts2 = self._create_timeslot(schedule=self.schedule_musikrotation)
        url = self._url("shows", self.show_musikrotation, "timeslots", ts2.id, "note")
        note = self._create_random_note_content(title="cool")
        res = client.post(url, note, format="json")
        self.assertEqual(res.status_code, 201)

    @skip("nested routes are deprecated")
    def test_notes_can_be_filtered_through_nested_routes_and_query_params(self):
        client = self._get_client()

        ts1 = self._create_timeslot(schedule=self.schedule_musikrotation)
        ts2 = self._create_timeslot(schedule=self.schedule_beatbetrieb)
        ts3 = self._create_timeslot(schedule=self.schedule_beatbetrieb)
        n1 = self._create_note(timeslot=ts1)
        n2 = self._create_note(timeslot=ts2)
        n3 = self._create_note(timeslot=ts3)

        def _get_ids(res):
            return set(ts["id"] for ts in res.data)

        # /shows/{pk}/notes/
        query_res = client.get(self._url("notes") + f"?showIds={self.show_beatbetrieb.id}")
        route_res = client.get(self._url("shows", self.show_beatbetrieb.id, "notes"))
        ids = {n2.id, n3.id}
        self.assertEqual(_get_ids(query_res), ids)
        self.assertEqual(_get_ids(route_res), ids)

        query_res = client.get(self._url("notes") + f"?showIds={self.show_musikrotation.id}")
        route_res = client.get(self._url("shows", self.show_musikrotation.id, "notes"))
        ids = {n1.id}
        self.assertEqual(_get_ids(query_res), ids)
        self.assertEqual(_get_ids(route_res), ids)

        # /shows/{pk}/timeslots/{pk}/note/
        query_res = client.get(self._url("notes") + f"?timeslot={ts2.id}")
        route_res = client.get(
            self._url("shows", self.show_beatbetrieb.id, "timeslots", ts2.id, "note")
        )
        ids = {n2.id}
        self.assertEqual(_get_ids(query_res), ids)
        self.assertEqual(_get_ids(route_res), ids)
