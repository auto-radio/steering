# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- `owners` optional field in `Host` model as many-to-many reference to `User` (steering#166)
- `language` and `topic` optional fields in `Timeslot` model (steering#169)

### Changed

- `note_id` and `show_id` are read-only fields of the `TimeslotSerializer` (steering#171)

### Removed

- `note_id` and `show` fields from the `Timeslot` model (steering#171)

## [1.0.0-alpha2] - 2023-06-20

### Added

- `Image` concrete model to handle all instances.
- API endpoint `/api/v1/images/` to add, update and delete images.
- Add `Host` admin

### Changed

- The `Host`, `Note`, `Show` models & serializers reference the new `Image`.
- The `logo` field in the `Show` model is a reference to `Image`.
- The "conflict resolution" methods of `Schedule` are now service functions.
- Update all APIs to return attributes / properties in camelCase notation (aura#141)
- Use `_id` suffix for all object reference in REST APIs (aura#166)
- Use blank strings instead of nullable strings in REST APIs (aura#167)
- Upgrade Poetry dependencies and Django to the next LTS version (steering#137)

### Removed

- The abstract `ModelWithImageFields` from the program models.
- The `ThumbnailsMixin` from the program serializers.
- The abstract `ModelWithCreatedUpdatedFields` from the program models.

### Fixed

- use kebab-case in URLs
- don’t output invalid PPOI format
- note image should allow null values
- don’t force REST API consumers to set `repetition_of` for timeslots
- The timeslot generation leaves the `last_date` unset if it is null.
- Note.tags attribute not updated via API (steering#157)
- make subtitle field in Category a CharField

## [1.0.0-alpha1] - 2023-02-24

Initial release.

