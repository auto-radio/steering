# Generated by Django 4.2.2 on 2023-07-27 19:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("program", "0066_license_delete_licensetype"),
    ]

    operations = [
        migrations.AddField(
            model_name="image",
            name="license",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="images",
                to="program.license",
            ),
        ),
    ]
