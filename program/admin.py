from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from program.models import (
    Category,
    FundingCategory,
    Host,
    Language,
    License,
    LinkType,
    MusicFocus,
    RRule,
    Topic,
    Type,
    UserProfile,
)


@admin.register(Category, FundingCategory, MusicFocus, Topic, Type)
class AdminWithNameSlugIsActive(admin.ModelAdmin):
    fields = ("is_active", "name", "slug")
    list_display = ("name", "slug", "is_active")


@admin.register(LinkType)
class LinkTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "type")


@admin.register(License)
class LicenseAdmin(admin.ModelAdmin):
    list_display = ("name", "identifier")


@admin.register(Host)
class HostAdmin(admin.ModelAdmin):
    fields = ("name", "email", "biography", "created_at", "created_by", "updated_at", "updated_by")
    list_display = ("name",)
    readonly_fields = ("created_at", "created_by", "updated_at", "updated_by")
    search_fields = ("name",)


@admin.register(Language)
class LanguageAdmin(admin.ModelAdmin):
    fields = ("is_active", "name")
    list_display = ("name", "is_active")


@admin.register(RRule)
class RRuleAdmin(admin.ModelAdmin):
    fields = ("name", "freq", "interval", "by_set_pos", "by_weekdays", "count")
    list_display = ("name", "freq", "interval", "by_set_pos", "by_weekdays", "count")


class UserProfileInline(admin.StackedInline):
    model = UserProfile
    fields = ("cba_username", "cba_user_token")
    can_delete = False
    verbose_name_plural = "Profile"
    fk_name = "user"


class UserProfileUserAdmin(UserAdmin):
    inlines = (UserProfileInline,)

    def get_queryset(self, request):
        """Let common users only edit their own profile"""
        if not request.user.is_superuser:
            return super(UserAdmin, self).get_queryset(request).filter(pk=request.user.id)

        return super(UserAdmin, self).get_queryset(request)

    def get_readonly_fields(self, request, obj=None):
        """Limit field access for common users"""
        if not request.user.is_superuser:
            return (
                "username",
                "is_staff",
                "is_superuser",
                "is_active",
                "date_joined",
                "last_login",
                "groups",
                "user_permissions",
            )
        return list()

    def get_inline_instances(self, request, obj=None):
        """Append profile fields to UserAdmin"""
        if not obj:
            return list()

        return super(UserProfileUserAdmin, self).get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, UserProfileUserAdmin)
