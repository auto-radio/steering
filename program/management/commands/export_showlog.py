# -*- coding: utf-8 -*-

import codecs
import sys
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from program.models import TimeSlot


class Command(BaseCommand):
    help = "export playlog for one year"
    args = "<year>"

    def handle(self, *args, **options):
        UTF8Writer = codecs.getwriter("utf8")
        sys.stdout = UTF8Writer(sys.stdout)

        if len(args) == 1:
            try:
                year = int(args[0])
            except ValueError:
                raise CommandError("'%s' is not a valid year" % args[0])
        else:
            raise CommandError("you must provide the year")

        self.stdout.write(self.style.NOTICE, f"# Radio Helsinki Sendungslog {year}")

        start = datetime(year, 1, 1, 0, 0)
        end = datetime(year + 1, 1, 1, 0, 0)

        currentDate = None
        for ts in (
            TimeSlot.objects.filter(end__gt=start, start__lt=end)
            .select_related("schedule")
            .select_related("show")
        ):
            if currentDate is None or currentDate < ts.start.date():
                if currentDate:
                    self.stdout.write("\n")
                currentDate = ts.start.date()
                self.stdout.write(
                    self.style.NOTICE, currentDate.strftime("## %a %d.%m.%Y:\n")
                )

            title = ts.show.name
            if ts.schedule.is_repetition:
                title += " (WH)"

            self.stdout.write(
                self.style.NOTICE,
                f' * **{ts.start.strftime("%H:%M:%S")} - {ts.end.strftime("%H:%M:%S")}**: {title}',
            )
