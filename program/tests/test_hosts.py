import pytest

from conftest import assert_data
from program.tests.factories import HostFactory

pytestmark = pytest.mark.django_db


def url(host=None) -> str:
    return f"/api/v1/hosts/{host.id}/" if host else "/api/v1/hosts/"


def host_data(image=None) -> dict[str, str | int]:
    data = {
        "biography": "BIOGRAPHY",
        "email": "host@aura.radio",
        "name": "NAME",
    }

    if image:
        data["image_id"] = image.id

    return data


def test_create_host(admin_api_client):
    data = host_data()

    response = admin_api_client.post(url(), data=data)

    assert response.status_code == 201

    assert_data(response, data)


def test_create_host_forbidden_for_common_user(common_api_client1):
    data = host_data()

    response = common_api_client1.post(url(), data=data)

    assert response.status_code == 403


def test_delete_host(admin_api_client, host):
    response = admin_api_client.delete(url(host))

    assert response.status_code == 204


def test_delete_host_forbidden_for_common_user(common_api_client1, host):
    response = common_api_client1.delete(url(host))

    assert response.status_code == 403


def test_list_hosts(admin_api_client):
    HOSTS = 3
    HostFactory.create_batch(size=HOSTS)

    response = admin_api_client.get(url())

    assert len(response.data) == HOSTS


def test_retrieve_host(api_client, host):
    response = api_client.get(url(host))

    assert response.status_code == 200


@pytest.mark.skip
def test_update_host(admin_api_client, host, image):
    update = host_data(image)
    update["is_active"] = False

    response = admin_api_client.put(url(host), data=update)

    assert response.status_code == 200

    assert_data(response, update)


def test_update_host_forbidden_for_common_user(common_api_client1, host):
    update = host_data()

    response = common_api_client1.put(url(host), data=update)

    assert response.status_code == 403
